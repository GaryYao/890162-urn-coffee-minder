/*
 * Ronnoco_coffee_urn.ino
 *
 * Created: 8/3/2017 4:19:35 PM
 * Author: Gary
 */ 

#include "data.h"
#include <EEPROM.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <TimerOne.h>
#include <LiquidCrystal_I2C_ST7032i.h>
#include <Wire.h>
#include <HX711.h>
//#include <LowPower.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/common.h>

//class instantiations
LiquidCrystal_I2C_ST7032i lcd(0x3E,8,2);
HX711 scale;

static long filteredCnt, lastCnt, stableCnt, noUrnCnt, emptyUrnCnt, fullUrnCnt;
static int fullUrnWeight;
int percentFull,lastPercentFull;
int steadyCnt;
long secTimer,LCDTimer;
int hour,minute;
char displayString[8]={};
static boolean hideTimer,noUrn;
static boolean lcdTimerFlag,coffeeTimerFlag;
unsigned long runTime,wake;
float calibrationWeight;
float dynamicCalibrationFactor;
float EEPROMCalibrationFactor;//read from EEPROM and add 100 assuming the calibration factor is around 200

uint8_t gauge0[8] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff};//full
//uint8_t gauge1[8] = {0xfe,0xfe,0xfe,0xfe,0xfe,0xfe,0xfe};
//uint8_t gauge2[8] = {0xfc,0xfc,0xfc,0xfc,0xfc,0xfc,0xfc};
//uint8_t gauge3[8] = {0xf8,0xf8,0xf8,0xf8,0xf8,0xf8,0xf8};
//uint8_t gauge4[8] = {0xf0,0xf0,0xf0,0xf0,0xf0,0xf0,0xf0};
uint8_t gauge5[8] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0};//empty
	
void tareScale(void)
{
	scale.tare();
	scale.set_offset(scale.get_offset()+SCALE_OFFSET);
	lcd.clear();
	lcd.writeLine(2,0,"Tared");
	delay(1500);
	lcd.clear();
}

void readSample(void)
{
	filteredCnt = scale.get_units();
}

boolean changed(long thisCnt)
{
	if(thisCnt < (lastCnt + HALF_WINDOW_SIZE))
	{
		if(thisCnt > (lastCnt - HALF_WINDOW_SIZE))
		{
			return false;
		}
	}
	return true;
}

void calculatePercentFull(void) {
	float rawPercent = ((float)filteredCnt-EMPTY_URN_WEIGHT)/(fullUrnWeight-EMPTY_URN_WEIGHT);
	percentFull = rawPercent*100;

	if(filteredCnt<600)
	{
		percentFull=-300;
	}
	Serial.println("Percent full: ");
	Serial.println(percentFull);
}

void initScale(void)
{
	scale.begin(5,6); // DOUT=5, SCK=6
	//delay(250);
	scale.tare();
}

void initLCD(void)
{
	pinMode(10,OUTPUT);
	digitalWrite(10,HIGH);
	
	pinMode(A0,OUTPUT);//Toggle the reset pin for the LCD
	digitalWrite(A0, HIGH);
	delay(500);
	digitalWrite(A0, LOW);
	delay(100);
	digitalWrite(A0, HIGH);
		
	lcd.init();           //  Initialize the lcd
	lcd.clear();          //  Clear the display
	
	lcd.createChar(0, gauge0);
	lcd.createChar(5, gauge5);
		
	lcd.setContrast(40);
}


void LCDSleepPrep(void)
{
	digitalWrite(A4,LOW);
	digitalWrite(A5,LOW);
	digitalWrite(A0,LOW);//reset pin
	digitalWrite(10,LOW);//power pin
}

/*
 * Function:  writeLine
 * --------------------
 * Writes a line of characters to the LCD
 *
 *  row: cursor row location
 *	col: cursor col location
 *	c[]: characters to be written
 *  returns: void
 */
 void LiquidCrystal_I2C_ST7032i::writeLine(uint8_t row, uint8_t col, char c[]){
	 setCursor(row,col);
	 for (uint8_t i=0; i<strlen(c);i++)
	 {
		 if (row<=7)
			write(c[i]);
		 row++;
	 }
 }
 
 /*
 * Function:  setCapacity
 * --------------------
 * 
 *	Indicate the pot fill level
 *  cap: 8->full
		 0->empty
 *  returns: void
 */
void LiquidCrystal_I2C_ST7032i::setCapacity(uint8_t cap){
	setCursor(0,1);
	for (uint8_t i=0; i<cap;i++)
	{
		lcd.write(0);
	}
	for (uint8_t i=cap; i<=8;i++)
	{
		lcd.write(5);
	}
}

void callback()
{
	noInterrupts();
	//secTimer=secTimer-1;
	coffeeTimerFlag=true;
	interrupts();
}

void setFullUrn()
{
	fullUrnWeight=scale.get_units();
}

void setCalibrationScale()
{
	calibrationWeight=scale.get_value(2);
	dynamicCalibrationFactor=calibrationWeight/CALIBRATION_WEIGHT;
	EEPROM.write(11,dynamicCalibrationFactor-100);
	EEPROMCalibrationFactor=EEPROM.read(11)+100;
	scale.set_scale(dynamicCalibrationFactor);
	Serial.print("calibrated Weight: ");
	Serial.println(calibrationWeight);
	Serial.print("Calibration factor: ");
	Serial.println(dynamicCalibrationFactor);
	Serial.print("EEPROM Calibration factor: ");
	Serial.println(EEPROMCalibrationFactor);
	
}

ISR(WDT_vect) {
	wdt_disable();
	wdt_reset();
	WDTCSR &= ~_BV(WDIE);
}

void sleepFor(uint8_t wdt_period) {
	wdt_enable(wdt_period);
	wdt_reset();
	WDTCSR |= _BV(WDIE);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_mode();
	wdt_disable();
	WDTCSR &= ~_BV(WDIE);
}

void setupLowPower() {
	//To reduce power, setup all pins as inputs with no pullups
	for(int x = 1 ; x < 18 ; x++){
		pinMode(x, INPUT);
		digitalWrite(x, LOW);
	}

	pinMode(3, INPUT); //This is the main button, tied to INT1
	digitalWrite(3, HIGH); //Enable internal pull up on button
	
	//Power down various bits of hardware to lower power usage
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sleep_enable();

	//Shut off ADC, TWI, SPI, Timer0, Timer1

	ADCSRA &= ~(1<<ADEN); //Disable ADC
	ACSR = (1<<ACD); //Disable the analog comparator
	DIDR0 = 0x3F; //Disable digital input buffers on all ADC0-ADC5 pins
	DIDR1 = (1<<AIN1D)|(1<<AIN0D); //Disable digital input buffer on AIN1/0
	
 	//power_twi_disable();
 	power_spi_disable();
 	//power_usart0_disable();
	//power_timer0_disable(); //Needed for delay_ms
	//power_timer1_disable();
	power_timer2_disable(); //Needed for asynchronous 32kHz operation

	//Setup TIMER2 HAS TO STAY IN EVEN THOUGH WE'RE NOT USING... causes 220uA+ drain!
	TCCR2A = 0x00;
	//TCCR2B = (1<<CS22)|(1<<CS20); //Set CLK/128 or overflow interrupt every 1s
	TCCR2B = (1<<CS22)|(1<<CS21)|(1<<CS20); //Set CLK/1024 or overflow interrupt every 8s
	ASSR = (1<<AS2); //Enable asynchronous operation
	TIMSK2 = (1<<TOIE2); //Enable the timer 2 interrupt

	//Setup external INT0 interrupt
	//   EICRA = (1<<ISC01); //Interrupt on falling edge
	//   EIMSK = (1<<INT0); //Enable INT0 interrupt

// 	attachInterrupt(1, btnISRasleep, FALLING);
// 	enteringSleep = false;
	
	sei(); //Enable global interrupts
}

//This is a not-so-accurate delay routine
//Calling fake_msdelay(100) will delay for about 100ms
//Assumes 8MHz clock
void fake_msdelay(int x){
	for( ; x > 0 ; x--)
	fake_usdelay(1000);
}

//This is a not-so-accurate delay routine
//Calling fake_usdelay(100) will delay for about 100us
//Assumes 8MHz clock
void fake_usdelay(int x){
	for( ; x > 0 ; x--) {
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
	}
}

void setup()
{
	setupLowPower();
	///* add setup code here, setup code runs once when the processor starts */
	Serial.begin(BAUDRATE);
	Serial.println(F("testing"));
	
	initLCD();
	Serial.println(F("LCD init"));
	initScale();
	tareScale();
	scale.set_scale(CALIBRATION_FACTOR); //calibration factor without calibration
	EEPROMCalibrationFactor=EEPROM.read(11)+100;
	scale.set_scale(EEPROMCalibrationFactor);
	Serial.println(F("scale init"));

	Timer1.initialize();
	Serial.println(F("Timer1 init"));
	
	pinMode(3, INPUT_PULLUP);
/*	attachInterrupt(digitalPinToInterrupt(3), tareScale, CHANGE);*/
	pinMode(2, INPUT_PULLUP);
/*	attachInterrupt(digitalPinToInterrupt(2), setFullUrn, CHANGE);*/
	Serial.println(F("2 buttons init"));
	//
	//
	//secTimer=COFFEE_TIMER;
	hideTimer=false;
	fullUrnWeight=FULL_URN_WEIGHT;
	lcdTimerFlag=false;
	coffeeTimerFlag=false;

	//scale.power_down();
	
//	Serial.println(F("Coffee urn minder init"));
	secTimer=COFFEE_TIMER;
// 	Serial.print("Init Timer:");
// 	Serial.println(secTimer);
	wake=millis();
}

void loop()
{
	//Timer1.attachInterrupt(callback); 

	//while(1)
	//{
		//coffeeTimerFlag=false;
 		//scale.power_up();
 		//readSample();
		//initLCD();
		//lcd.writeLine(0,0,"test");
		//Serial.println(F("write to LCD"));
		//Serial.println(filteredCnt);
		//Serial.println(runTime);
		//delay(50);		
		//scale.power_down();
		//LCDSleepPrep();
		//runTime=millis()-wake;
		//secTimer-=(runTime+2000);
		//sleepFor(WDTO_2S);
		//wake=millis();
		//Serial.print("wake:");
		//Serial.println(wake);
		//Serial.print("Timer:");
		//Serial.println(secTimer);
	//}

	/* add main program code here, this code starts again each time it ends */
	Serial.println(scale.get_units(),0.1);
	delay(100);
	Serial.print("State: ");
	Serial.println(state);
// 	Serial.print("Full urn weight: ");
// 	Serial.print(fullUrnWeight);
	if(digitalRead(2)==LOW)
	{
		setCalibrationScale();
		Serial.println("Set calibration factor");
	}
	if(digitalRead(3)==LOW)
	{
		tareScale();
		Serial.println("Scale tared.");
	}
	
	switch(state)
	{
		case setupState:
			state = fluxState;
			steadyCnt = 0;
			break;
		
		case steadyState:
			readSample();
			
			if(changed(filteredCnt)){
				state = fluxState;
				steadyCnt = 0;
			}
			else
			{
				scale.power_down();
				runTime=millis()-wake;
				secTimer = secTimer - (runTime+2000+133);//added 133 for compensation
				sleepFor(WDTO_2S);
				wake=millis();
				scale.power_up();
// 				Serial.println("Wake from sleep and the timer is");
// 				Serial.println(secTimer);		
			}
			break;
			
		case fluxState:
			readSample();
			if(changed(filteredCnt)){
				steadyCnt = 0;
			}
			else {
				steadyCnt++;
				if(steadyCnt > 3){
					state = reportState;
				}
			}
			break;
			
		case NoUrnState:
			readSample();
			if(changed(filteredCnt)){
				state = fluxState;
				steadyCnt = 0;
			}
			scale.power_down();
			if (LCDTimer<0)
			{
				LCDSleepPrep();
				Serial.println("LCD sleep");
			}
// 			else
// 			{
// 				initLCD();
// 				lcd.writeLine(1,0,"No urn");
// 			}
			runTime=millis()-wake;
			LCDTimer = LCDTimer - (runTime+2000L+133L);//added 133 for compensation
			sleepFor(WDTO_2S);
			wake=millis();
			scale.power_up();
			//Serial.println("Wake from LCD sleep and the timer is");
			//Serial.println(LCDTimer);
			//Serial.println("Runtime");
			//Serial.println(runTime);
			break;
			
		case ExpiredUrnState:
			readSample();
			if(changed(filteredCnt)){
				state = fluxState;
				steadyCnt = 0;
			}
			scale.power_down();
			if (LCDTimer<0)
			{
				LCDSleepPrep();
				Serial.println("LCD sleep");
			}
// 			else
// 			{
// 				initLCD();
// 				lcd.writeLine(0,0,"Expired");
// 			}
			runTime=millis()-wake;
			LCDTimer = LCDTimer - (runTime+2000L+133L);//added 133 for compensation
			sleepFor(WDTO_2S);
			wake=millis();
			scale.power_up();
			secTimer=COFFEE_TIMER;
			hideTimer=true;
			//Serial.println("Wake from LCD sleep and the timer is");
			//Serial.println(LCDTimer);
			//Serial.println("Runtime");
			//Serial.println(runTime);
			break;
			
		case EmptyUrnState:
			readSample();
			if(changed(filteredCnt)){
				state = fluxState;
				steadyCnt = 0;
			}
			scale.power_down();
			if (LCDTimer<0)
			{
				LCDSleepPrep();
				Serial.println("LCD sleep");
			}
// 			else
// 			{
// 				initLCD();
// 				lcd.writeLine(2,0,"Empty");
// 			}
			runTime=millis()-wake;
			LCDTimer = LCDTimer - (runTime+2000L+133L);//added 133 for compensation
			sleepFor(WDTO_2S);
			wake=millis();
			scale.power_up();
			//Serial.println("Wake from LCD sleep and the timer is");
			//Serial.println(LCDTimer);
			//Serial.println("Runtime");
			//Serial.println(runTime);
			break;
						
		case reportState:
			initLCD();
			readSample();
			calculatePercentFull();
			//Serial.println(percentFull);
			sprintf(displayString,"%d",percentFull);
			//add code to display to LCD
			lcd.clear();
			if(percentFull>5 && percentFull<12)
			{
				lcd.setCapacity(1);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);
				Serial.println("1 bar");
				state = steadyState;
			}
			else if (percentFull>=12 && percentFull<24)
			{
				lcd.setCapacity(2);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);	
				Serial.println("2 bar");	
				state = steadyState;		
			}
			else if (percentFull>=24 && percentFull<36)
			{
				lcd.setCapacity(3);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);
				Serial.println("3 bar");
				state = steadyState;
			}
			else if (percentFull>=36 && percentFull<48)
			{
				lcd.setCapacity(4);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);				
				Serial.println("4 bar");
				state = steadyState;
			}
			else if (percentFull>=48 && percentFull<60)
			{
				lcd.setCapacity(5);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);
				Serial.println("5 bar");
				state = steadyState;
			}
			else if (percentFull>=60 && percentFull<72)
			{
				lcd.setCapacity(6);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);
				Serial.println("6 bar");
				state = steadyState;
			}
			else if (percentFull>=72 && percentFull<84)
			{
				lcd.setCapacity(7);
				hideTimer=false;
				//lcd.writeLine(0,1,displayString);
				Serial.println("7 bar");
				state = steadyState;
			}
			else if (percentFull >=84)
			{
				lcd.setCapacity(8);
				//lcd.writeLine(0,1,displayString);
				Serial.println("8 bar");
				//Timer1.attachInterrupt(callback); 
				if(lastPercentFull<-100)//condition for resetting the timer(remove the urn)
					secTimer=COFFEE_TIMER;
				hideTimer=false;
				state = steadyState;
			}			
			else if (percentFull >= -10 && percentFull <=5)
			{
				lcd.setCapacity(0);
				//lcd.writeLine(0,1,displayString);
				Serial.println("0 bar");
				lcd.writeLine(2,0,"Empty");
				hideTimer=true;
				state = EmptyUrnState;
				LCDTimer=LCD_TIMER;
			}
			else
			{
				lcd.setCapacity(0);
				lcd.writeLine(1,0,"No urn");
				hideTimer=true;
				state=NoUrnState;
				LCDTimer=LCD_TIMER;
			}
			//state = steadyState;
			lastPercentFull=percentFull;
			break;	
	}
	lastCnt=filteredCnt;
	if(secTimer<0 && hideTimer==false)
	{
		lcd.writeLine(0,0,"Expired");
		state=ExpiredUrnState;
		LCDTimer=LCD_TIMER;
	}
	else
	{
		if (hideTimer==false)
		{
			hour=secTimer/3600000;
			minute=secTimer/60000%60;
			
			memset(displayString, 0, sizeof displayString);
   			sprintf(displayString,"%d",hour);
  			lcd.writeLine(2,0,displayString);
   			lcd.write(0x3A);//colon
		   
			memset(displayString, 0, sizeof displayString); 
   			sprintf(displayString,"%02d",minute);
   			lcd.writeLine(4,0,displayString);
		}
	}
	if(coffeeTimerFlag==true)
	{
		secTimer=secTimer-1000;
		coffeeTimerFlag=false;
	}
	//delay(100);
}
