#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/common.h>

ISR(WDT_vect) {
	wdt_disable();
	wdt_reset();
	WDTCSR &= ~_BV(WDIE);
}

void sleepFor(int seconds) {
	uint8_t wdt_period;
	
	if(seconds == 0){
		return;
	}
	
	powerLoadcell(LOW);
	ADS1230_Power_Down_Mode();
	
	while(seconds > 0){
		if(seconds >= 8) {
			wdt_period = WDTO_8S;
			seconds -= 8;
		}
		else if(seconds >= 4) {
			wdt_period = WDTO_4S;
			seconds -= 4;
		}
		else if (seconds >= 2) {
			wdt_period = WDTO_2S;
			seconds -= 2;
		}
		else if(seconds >= 1) {
			wdt_period = WDTO_1S;
			seconds -= 1;
		}
		
		wdt_enable(wdt_period);
		wdt_reset();
		WDTCSR |= _BV(WDIE);
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		sleep_mode();
		wdt_disable();
		WDTCSR &= ~_BV(WDIE);
	}
	ADS1230_Wakeup();
	powerLoadcell(HIGH);
	
	//attempt to wake load cell and sleep while converting
	wdt_enable(WDTO_500MS);
	wdt_reset();
	WDTCSR |= _BV(WDIE);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_mode();
	wdt_disable();
	WDTCSR &= ~_BV(WDIE);
	
	//necessary delay to allow A2D conversion to finish before read
	//delay(60); //delay no longer necessary when going back to sleep for half a second after waking the load cell
}
//This is a not-so-accurate delay routine
//Calling fake_msdelay(100) will delay for about 100ms
//Assumes 8MHz clock
void fake_msdelay(int x){
	for( ; x > 0 ; x--)
	fake_usdelay(1000);
}

//This is a not-so-accurate delay routine
//Calling fake_usdelay(100) will delay for about 100us
//Assumes 8MHz clock
void fake_usdelay(int x){
	for( ; x > 0 ; x--) {
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
		__asm__("nop\n\t");
	}
}


void setupLowPower(void) {
	//To reduce power, setup all pins as inputs with no pullups
	for(int x = 1 ; x < 18 ; x++){
		pinMode(x, INPUT);
		digitalWrite(x, LOW);
	}
	
	//   pinMode(2, INPUT);
	//   digitalWrite(2, HIGH);
// 	pinMode(3, INPUT); //This is the main button, tied to INT1
// 	digitalWrite(3, HIGH); //Enable internal pull up on button
	
	//Power down various bits of hardware to lower power usage
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sleep_enable();

	//Shut off ADC, TWI, SPI, Timer0, Timer1

	ADCSRA &= ~(1<<ADEN); //Disable ADC
	ACSR = (1<<ACD); //Disable the analog comparator
	//DIDR0 = 0x3F; //Disable digital input buffers on all ADC0-ADC5 pins
	DIDR1 = (1<<AIN1D)|(1<<AIN0D); //Disable digital input buffer on AIN1/0
	
	power_twi_disable();
	//power_spi_disable();
	
	//power_usart0_disable();
	
	//power_timer0_disable(); //Needed for delay_ms
	power_timer1_disable();
	//power_timer2_disable(); //Needed for asynchronous 32kHz operation

	//Setup TIMER2 HAS TO STAY IN EVEN THOUGH WE'RE NOT USING... causes 220uA+ drain!
	TCCR2A = 0x00;
	//TCCR2B = (1<<CS22)|(1<<CS20); //Set CLK/128 or overflow interrupt every 1s
	TCCR2B = (1<<CS22)|(1<<CS21)|(1<<CS20); //Set CLK/1024 or overflow interrupt every 8s
	ASSR = (1<<AS2); //Enable asynchronous operation
	TIMSK2 = (1<<TOIE2); //Enable the timer 2 interrupt

	//Setup external INT0 interrupt
	//   EICRA = (1<<ISC01); //Interrupt on falling edge
	//   EIMSK = (1<<INT0); //Enable INT0 interrupt
	//attachInterrupt(1, accelISR, FALLING);

	sei(); //Enable global interrupts
}

