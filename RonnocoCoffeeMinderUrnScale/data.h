#ifndef DATA_H_
#define DATA_H_
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#include <Arduino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include <TimerOne.h>

//defines for easy tweaking
#define HALF_WINDOW_SIZE 5 //100
#define STEADY_STATE_SLEEP_SECONDS	4
#define FLUX_STATE_SLEEP_SECONDS	1
#define BAUDRATE 115200
#define CALIBRATION_FACTOR 229 //for 10g
//-22700 for kg
#define COFFEE_TIMER 60L*60L*2L*1000L //in milliseconds 
//#define COFFEE_TIMER 60L*1000L //in milliseconds 
#define LCD_TIMER 10L*60L*1000L // LCD shutoff after no urn detected in milliseconds.
//#define LCD_TIMER 60L*1000L // LCD shutoff after no urn detected in milliseconds.
#define EMPTY_URN_WEIGHT (float)620
//#define FULL_URN_WEIGHT (float)256
#define FULL_URN_WEIGHT (float)1238
#define CALIBRATION_WEIGHT 90//900 grams
#define SCALE_OFFSET 13511L



//States
typedef enum {
	setupState,			//0
	steadyState,		//1
	fluxState,			//2
	reportState,		//3
	NoUrnState,		//4
	EmptyUrnState,	//5
	ExpiredUrnState	//6
} state_t;

state_t state;

#endif